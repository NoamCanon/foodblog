﻿using FoodBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodBlog.DAL
{
    public class ProjectInitializer : System.Data.Entity.DropCreateDatabaseAlways<ProjectContext>
    {
        protected override void Seed(ProjectContext context)
        {
            var Cheffs = new List<Cheff>
            {
                new Cheff
                {
                    ID=1,
                    Name="Sali Nibaba",
                    NickName="the best",
                    BirthDay = new DateTime(1997, 5, 9),
                    PlaceOfBirth = Country.India
                },
                new Cheff
                {
                    ID=2,
                    Name="Steffan Solley",
                    NickName="France Cooking Leader",
                    BirthDay = new DateTime(1994, 2, 1),
                    PlaceOfBirth = Country.France
                },
                new Cheff
                {
                    ID=3,
                    Name="Rachel Klein",
                    NickName="Racheli",
                    BirthDay = new DateTime(1964, 3, 4),
                    PlaceOfBirth = Country.USA
                },
                 new Cheff
                {
                    ID=4,
                    Name="Asaf Granit",
                    NickName="TV shows Cheff",
                    BirthDay = new DateTime(1974, 4, 4),
                    PlaceOfBirth = Country.Israel
                },
                 new Cheff
                {
                    ID=5,
                    Name="Alan Wong",
                    NickName="The magician",
                    BirthDay = new DateTime(1964, 2, 9),
                    PlaceOfBirth = Country.GreatBritain
                },
                 new Cheff
                {
                    ID=6,
                    Name="Alex Garcia",
                    NickName="Wow guy",
                    BirthDay = new DateTime(1954, 5, 11),
                    PlaceOfBirth = Country.Israel
                },
                 new Cheff
                {
                    ID=7,
                    Name="Yaniv Ohayon",
                    NickName="A+",
                    BirthDay = new DateTime(1997, 4, 8),
                    PlaceOfBirth = Country.France
                },
                 new Cheff
                {
                    ID=8,
                    Name="Noam Canon",
                    NickName="CheffDeveloper",
                    BirthDay = new DateTime(1993, 11, 2),
                    PlaceOfBirth = Country.India
                },
                 new Cheff
                {
                    ID=9,
                    Name="Noam Danino",
                    NickName="Crazy",
                    BirthDay = new DateTime(1969, 7, 7),
                    PlaceOfBirth = Country.GreatBritain
                },
            };
            Cheffs.ForEach(h => context.Cheffs.Add(h));
            context.SaveChanges();

            var recipes = new List<Recipe>
            {
                new Recipe {
                    RecipeID = 1,
                    Name = "Chocolate Cake",
                    CheffID = Cheffs[0].ID,
                    PublishDate=new DateTime(2018, 5, 9),
                    CreationProcess="Add some eggs, salt, suger. mix it all and finally put in the oven"
                },
                new Recipe  {
                    RecipeID = 2,
                    Name = "Sushi",
                    CheffID = Cheffs[1].ID,
                    PublishDate=new DateTime(2015, 1, 8),
                    CreationProcess="Get some Rise and Salmon and put it both together"
                },
                new Recipe  {
                    RecipeID = 3,
                    Name = "Pizza",
                    CheffID = Cheffs[2].ID,
                    PublishDate=new DateTime(2016, 2, 2),
                    CreationProcess="Create dough, put on it tomatos and cheese, then cook it"
                },
                new Recipe  {
                    RecipeID = 4,
                    Name = "Salmon Dish",
                    CheffID = Cheffs[3].ID,
                    PublishDate=new DateTime(2000, 3, 3),
                    CreationProcess="Get a fresh salmon and put it in the oven, add some oil"
                },
                new Recipe  {
                    RecipeID = 4,
                    Name = "French Fries",
                    CheffID = Cheffs[4].ID,
                    PublishDate=new DateTime(2001, 12, 12),
                    CreationProcess="Cutt the potatos and fry it"
                }
            };

            recipes.ForEach(r => context.Recipes.Add(r));
            context.SaveChanges();

            var comments = new List<Comment>
            {
                new Comment
                {
                    CommentID=1,
                    RecipeID=1,
                    Title="OMG!",
                    AuthorName="Food Addicted",
                    Content="It is the best!",
                },
                new Comment
                {
                    CommentID=2,
                    RecipeID=1,
                    Title="Unrecommended",
                    AuthorName="Food Addicted",
                    Content="It is the worst!",
                },
                new Comment
                {
                    CommentID=3,
                    RecipeID=2,
                    Title="AWESOME",
                    AuthorName="Yariv Cohen",
                    Content="I was very satisfied",
                },
            };
            comments.ForEach(c => context.Comments.Add(c));
            context.SaveChanges();

            var maps = new List<Map>();

            for (int i = 0; i < Cheffs.ToArray().Length; i++)
            {
                Map map = new Map
                {
                    MapID = Cheffs[i].ID,
                    Name = Cheffs[i].Name,
                    Address = Cheffs[i].PlaceOfBirth.ToString(),
                };

                maps.Add(map);
            }
            maps.ForEach(m => context.Maps.Add(m));
            context.SaveChanges();

        }
    }
}